<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@home');
Route::get('/informasi', 'InformasiController@show');
Route::get('/editor', 'EditorController@index');
Route::get('/jurnal', 'JurnalController@index');
Route::get('/reviewer', 'ReviewerController@index');
Route::get('/penilaian', 'PenilaianController@index');
