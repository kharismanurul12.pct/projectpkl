<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function dashboard()
    {
        return view('dashboard');
    }

    public function informasi()
    {
        return view('informasi');
    }

    public function editor()
    {
        return view('editor');
    }

    public function jurnal()
    {
        return view('jurnal');
    }

    public function reviewer()
    {
        return view('reviewer');
    }

    public function penilaian()
    {
        return view('penilaian');
    }
}
